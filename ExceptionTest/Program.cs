﻿using System;
using System.Threading;
using CaryDN;
using System.Windows.Forms;
using CaryDN.SCary32;

namespace ConsoleApp1
{
    internal class Program
    {
        #region Static Fields and Constants

        private static InstrumentEx CaryEx;

        #endregion

        #region All other members

        [STAThread]
        private static void Main()
        {
            Console.WriteLine("");
            CaryEx = new InstrumentEx();
            CaryEx.Registration.Key = "B7405-43748-8CC19-CC176-67F60";            

            if (!CaryEx.Connect())
            {
                Console.WriteLine("CaryEx.Connect() failed");
                Console.ReadKey();
                Environment.Exit(1);
            }
            Thread.Sleep(1000);
            Console.WriteLine("Pre-reading...");
            Read(CaryEx.Wavelength);
            Console.WriteLine("Read Complete. moving to 5000...");
            Scary.AccySet(CaryEx.WindowHandle, 0, 5000);
            CaryEx.Accessory.WaitNotBusy();

            var counter = 0;
            var position = 5000;
            var wavelength = 190f;
            CaryEx.Wavelength = wavelength;
            while (true)
                try
                {
                    Console.Write($"Counter: {++counter}");
                    var val = Read(wavelength);
                    Console.WriteLine($"\tValue: {val:F6}\tPosition: {position}\tWavelength: {CaryEx.Wavelength}");
                    position -= 5;
                    position = position < 4000 ? 5000 : position;
                    Scary.AccySet(CaryEx.WindowHandle, 0, position);
                    CaryEx.Accessory.WaitNotBusy();
                    wavelength += 10;
                    wavelength = wavelength > 1100 ? 190 : wavelength;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"\n\nError: {e}\n\nPress any key to exit...");
                    Console.ReadKey();
                    Environment.Exit(1);
                }

            // ReSharper disable once FunctionNeverReturns
        }

        private static float Read(float wavelength)
        {
            try
            {
                if (CaryEx.IsBusy)
                    CaryEx.Stop();

                if (CaryEx.IsScanning)
                {
                    CaryEx.Stop();
                    CaryEx.WaitNotScanning();
                }

                CaryEx.Setup();

                var frontBeam = CaryEx.Read(wavelength);
                CaryEx.WaitNotBusy();

                return frontBeam;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        #endregion
    }
}